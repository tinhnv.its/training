<?php

if (!defined('TABLE_NAME_USERS')) {
    define('TABLE_NAME_USERS', 'mst_users');
}

if (!defined('TABLE_NAME_PRODUCT')) {
    define('TABLE_NAME_PRODUCT', 'mst_product');
}

if (!defined('TABLE_NAME_SHOP')) {
    define('TABLE_NAME_SHOP', 'mst_shop');
}

if (!defined('TABLE_NAME_CUSTOMER')) {
    define('TABLE_NAME_CUSTOMER', 'mst_customer');
}

if (!defined('TABLE_NAME_ORDER')) {
    define('TABLE_NAME_ORDER', 'mst_order');
}

if (!defined('TABLE_NAME_ORDER_DETAIL')) {
    define('TABLE_NAME_ORDER_DETAIL', 'mst_order_detail');
}

if (!defined('IS_DELETE')) {
    define('IS_DELETE', 1);
}

if (!defined('IS_ACTIVE')) {
    define('IS_ACTIVE', 1);
}

if (!defined('IS_DEACTIVATE')) {
    define('IS_DEACTIVATE', 0);
}

if (!defined('TTL_REMEMBER_ME')) {
    define('TTL_REMEMBER_ME', 525600); // 1 year = 365*24*60
}

if (!defined('GROUP_ROLE')) {
    define('GROUP_ROLE', ['Admin', 'Reviewer', 'Editor']);
}

if (!defined('PAGE_LIMIT')) {
    define('PAGE_LIMIT', 10);
}

if (!defined('FOLDER_IMAGES')) {
    define('FOLDER_IMAGES', 'public/images');
}

if (!defined('PRODUCT_ID_DEFAULT')) {
    define('PRODUCT_ID_DEFAULT', 'S000000001');
}
