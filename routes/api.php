<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);

    Route::group([
        'middleware' => ['api', 'auth:api']
    ], function () {
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/refresh', [AuthController::class, 'refresh']);
    });
});

Route::group(['middleware' => ['api', 'auth:api']], function () {
    Route::group(['prefix' => 'products'], function () {
        Route::get('/', [ProductController::class, 'index'])->name('product.list');
        Route::get('/{id}', [ProductController::class, 'show'])->name('product.detail');
        Route::post('/', [ProductController::class, 'store'])->name('product.store');
        Route::post('/{id}', [ProductController::class, 'update'])->name('product.update');
        Route::delete('/{id}', [ProductController::class, 'destroy'])->name('product.delete');
    });

    Route::group(['prefix' => 'customers'], function () {
        Route::get('/export', [CustomerController::class, 'export'])->name('customer.export');
        Route::post('/import', [CustomerController::class, 'import'])->name('customer.import');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::put('/update-status/{id}', [UserController::class, 'updateStatus'])->name('user.update.status');
    });

    Route::resources([
        // 'products' => ProductController::class,
        'customers' => CustomerController::class,
        'users' => UserController::class,
        'orders' => OrderController::class,
    ]);
});
