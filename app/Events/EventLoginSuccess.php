<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EventLoginSuccess
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userId;
    public $lastLoginIp;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $lastLoginIp)
    {
        $this->userId = $userId;
        $this->lastLoginIp = $lastLoginIp;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('update-last-login');
    }
}
