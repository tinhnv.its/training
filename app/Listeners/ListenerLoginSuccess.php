<?php

namespace App\Listeners;

use App\Events\EventLoginSuccess;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class ListenerLoginSuccess implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\EventLoginSuccess  $event
     * @return void
     */
    public function handle(EventLoginSuccess $event)
    {
        DB::table(TABLE_NAME_USERS)
                ->where('id', $event->userId)
                ->update([
                    'last_login_at' => Carbon::now(),
                    'last_login_ip' => $event->lastLoginIp
                ]);
    }
}
