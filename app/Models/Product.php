<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = TABLE_NAME_PRODUCT;
    protected $primaryKey = 'product_id';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'product_name',
        'product_image',
        'product_price',
        'is_sales',
        'is_delete',
        'description',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [];

    /**
     * Enables us to hook into model event's
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $productId = $model->max('product_id');
            // if table empty
            if (!$productId) {
                $productId = PRODUCT_ID_DEFAULT;
            } else {
                $productId = ++$productId;
            }
            $model->product_id = $productId;
        });
    }
}
