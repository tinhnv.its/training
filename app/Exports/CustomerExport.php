<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;

class CustomerExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithEvents
{
    public function __construct(Collection $customers)
    {
        $this->customers = $customers;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->customers;
    }

    /**
    * @var Invoice $invoice
    */
    public function map($customer): array
    {
        return [
            $customer->customer_name,
            $customer->email,
            $customer->tel_num,
            $customer->address,
        ];
    }

    public function headings(): array
    {
        return [
            'Tên khác hàng',
            'Email',
            'TelNum',
            'Địa chỉ',
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1:D1')->getFill()->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'fillType' => 'solid',
                    'rotation' => 0,
                    'color' => [
                        'rgb' => 'CCCCCC'
                    ],
                ]);
            }
        ];
    }
}
