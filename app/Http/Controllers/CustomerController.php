<?php

namespace App\Http\Controllers;

use App\Exports\CustomerExport;
use App\Http\Requests\Customer\CreateCustomerRequest;
use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\Imports\CustomersImport;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers = Customer::orderBy('customer_id', 'DESC');

        if ($request->name) {
            $customers->where('customer_name', 'like', '%' . $request->name . '%');
        }
        if ($request->email) {
            $customers->where('email', 'like', '%' . $request->email . '%');
        }
        if ($request->is_active !== null) {
            $customers->where('is_active', $request->is_active);
        }
        if ($request->address) {
            $customers->where('address', 'like', '%' . $request->address . '%');
        }

        $customers = $customers->paginate(PAGE_LIMIT);

        return responseJson(Response::HTTP_OK, $customers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();
        $customer = Customer::create($input);
        return responseJson(Response::HTTP_OK, $customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        if (is_null($customer) || $customer->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }
        return responseJson(Response::HTTP_OK, $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
        $customer = Customer::find($id);
        if (is_null($customer) || $customer->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }

        if ($request->customer_name) {
            $customer->customer_name = $request->customer_name;
        }
        if ($request->email) {
            $customer->email = $request->email;
        }
        if ($request->tel_num) {
            $customer->tel_num = $request->tel_num;
        }
        if ($request->address) {
            $customer->address = $request->address;
        }
        if ($request->is_active !== null) {
            $customer->is_active = $request->is_active;
        }

        $customer->save();

        return responseJson(Response::HTTP_OK, $customer);
    }

    public function export(Request $request)
    {
        $onlyFirstPage = true;
        $customers = Customer::select('customer_name', 'email', 'tel_num', 'address')->orderBy('customer_id', 'DESC');

        if ($request->name) {
            $customers->where('customer_name', 'like', '%' . $request->name . '%');
            $onlyFirstPage = false;
        }
        if ($request->email) {
            $customers->where('email', 'like', '%' . $request->email . '%');
            $onlyFirstPage = false;
        }
        if ($request->is_active) {
            $customers->where('is_active', $request->is_active);
            $onlyFirstPage = false;
        }
        if ($request->address) {
            $customers->where('address', 'like', '%' . $request->address . '%');
            $onlyFirstPage = false;
        }

        if ($onlyFirstPage) {
            $customers = $customers->limit(PAGE_LIMIT)->get();
        } else {
            $customers = $customers->get();
        }

        $fileName = 'excel/customers/customers_' . Date('YmdHis') . '.xlsx';
        Excel::store(new CustomerExport($customers), $fileName, 'public');
        $fileUrl = env('APP_URL').Storage::url('public/' . $fileName);

        return responseJson(Response::HTTP_OK, $fileUrl);
    }

    public function import(Request $request)
    {
        try {
            $fileCustomers = $request->file('file');
            Excel::import(new CustomersImport, $fileCustomers);
        } catch (ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }
            return responseJson(Response::HTTP_UNPROCESSABLE_ENTITY, $failures);
        }
        return responseJson(Response::HTTP_OK, true);
    }
}
