<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::where('is_delete', !IS_DELETE)->orderBy('product_id', 'DESC');

        if ($request->product_name) {
            $products->where('product_name', 'like', '%' . $request->product_name . '%');
        }
        if ($request->product_price_from) {
            $products->where('product_price', '>=', $request->product_price_from);
        }
        if ($request->product_price_to) {
            $products->where('product_price', '<=', $request->product_price_to);
        }
        if ($request->is_sales !== null) {
            $products->where('is_sales', $request->is_sales);
        }

        $products = $products->paginate(PAGE_LIMIT);

        $items = $products->items();
        foreach ($items as &$item) {
            if (!empty($item->product_image)) {
                $item->product_image = env('APP_URL').Storage::url($item->product_image);
            } else {
                $item->product_image = env('APP_URL').'/images/no_image.png';
            }
        }

        return responseJson(Response::HTTP_OK, $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();
        if ($image = $request->file('image')) {
            $input['product_image'] = Storage::put(FOLDER_IMAGES.'/products', $image);
        }

        $product = Product::create($input);
        $product->product_image = env('APP_URL').Storage::url($product->product_image);

        return responseJson(Response::HTTP_OK, $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if (is_null($product) || $product->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }
        if (!empty($product->product_image)) {
            $product->product_image = env('APP_URL').Storage::url($product->product_image);
        }

        return responseJson(Response::HTTP_OK, $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        DB::beginTransaction();
        $product = Product::find($id);
        try {
            if (is_null($product) || $product->is_delete === IS_DELETE) {
                return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
            }

            if ($request->product_name) {
                $product->product_name = $request->product_name;
            }
            if ($request->product_image) {
                if ($image = $request->file('image')) {
                    // delete file image
                    Storage::delete($product->product_image);
                    $product->product_image = Storage::put(FOLDER_IMAGES.'/products', $image);
                }
            } else {
                if ($product->product_image) {
                    // delete file image
                    Storage::delete($product->product_image);
                    $product->product_image = null;
                }
            }
            if ($request->product_price) {
                $product->product_price = $request->product_price;
            }
            if ($request->is_sales) {
                $product->is_sales = $request->is_sales;
            }
            if ($request->description) {
                $product->description = $request->description;
            }

            $product->save();
            $product->product_image = env('APP_URL').Storage::url($product->product_image);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return responseJson(Response::HTTP_NOT_FOUND, $e->getMessage());
        }

        return responseJson(Response::HTTP_OK, $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $product = Product::find($id);
            Storage::delete($product->product_image);
            if (is_null($product) || $product->is_delete === IS_DELETE) {
                throw new \Exception("Không thể xoá sản phẩm.", Response::HTTP_SERVICE_UNAVAILABLE);
            }
            $product->is_delete = IS_DELETE;
            $product->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return responseJson(Response::HTTP_SERVICE_UNAVAILABLE, $e->getMessage());
        }
        return responseJson(Response::HTTP_OK, true);
    }
}
