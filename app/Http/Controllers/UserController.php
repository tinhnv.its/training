<?php
namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateStatusUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::where('is_delete', !IS_DELETE)->orderBy('id', 'DESC');

        if ($request->name) {
            $users->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->email) {
            $users->where('email', 'like', '%' . $request->email . '%');
        }
        if ($request->group_role) {
            $users->where('group_role', $request->group_role);
        }
        if ($request->is_active !== null) {
            $users->where('is_active', $request->is_active);
        }

        $users = $users->paginate(PAGE_LIMIT);

        return responseJson(Response::HTTP_OK, $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $user = User::create($input);
        return responseJson(Response::HTTP_OK, $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (is_null($user) || $user->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }
        return responseJson(Response::HTTP_OK, $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::find($id);
        if (is_null($user) || $user->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }

        if ($request->name) {
            $user->name = $request->name;
        }
        if ($request->email) {
            $user->email = $request->email;
        }
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }
        if ($request->is_active !== null) {
            $user->is_active = $request->is_active;
        }
        if ($request->group_role) {
            $user->group_role = $request->group_role;
        }

        $user->save();

        return responseJson(Response::HTTP_OK, $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (is_null($user) || $user->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }
        $user->is_delete = IS_DELETE;
        $user->save();
        return responseJson(Response::HTTP_OK, true);
    }

    /**
     * Update status the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(UpdateStatusUserRequest $request, $id)
    {
        $user = User::find($id);
        if (is_null($user) || $user->is_delete === IS_DELETE) {
            return responseJson(Response::HTTP_NOT_FOUND, __('response.404'));
        }
        $user->is_active = $request->is_active;
        $user->save();

        return responseJson(Response::HTTP_OK, $user);
    }
}
