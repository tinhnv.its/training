<?php

namespace App\Http\Controllers;

use App\Events\EventLoginSuccess;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Auth\LoginRequest;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        if ($request->remember_me) {
            $token = auth()->setTTL(TTL_REMEMBER_ME)->attempt(
                ['email' => $request->email, 'password' => $request->password, 'is_active' => IS_ACTIVE, 'is_delete' => !IS_DELETE],
                $request->remember_me
            );
        } else {
            $token = auth()->setTTL(env('JWT_TTL', 60))->attempt(
                ['email' => $request->email, 'password' => $request->password, 'is_active' => IS_ACTIVE, 'is_delete' => !IS_DELETE]
            );
        }

        if (!$token) {
            return responseJson(Response::HTTP_NOT_FOUND, __('auth.login_wrong'));
        }

        event(new EventLoginSuccess(auth()->user()->id, request()->ip()));

        return $this->generateToken($token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return responseJson(Response::HTTP_OK, []);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->generateToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function generateToken($token)
    {
        return responseJson(Response::HTTP_OK, [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL(),
            'user' => auth()->user()
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        return response()->json(auth()->user());
    }
}
