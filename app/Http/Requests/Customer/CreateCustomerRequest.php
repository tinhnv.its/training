<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\BaseFormRequest;

class CreateCustomerRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required|string|between:5,255',
            'email' => 'required|string|email|max:255',
            'tel_num' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:11',
            'address' => 'required|string|max:255',
            'is_active' => 'required|integer|between:0,1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_name.required' => __('validation.required', ['attribute' => 'tên']),
            'customer_name.between' => __('validation.between.string', ['attribute' => 'tên', 'min' => 5, 'max'=> 255]),

            'email.required' => __('validation.required', ['attribute' => 'email']),
            'email.email' => __('validation.email', ['attribute' => 'email']),
            'email.max' => __('validation.max.string', ['attribute' => 'email', 'max' => 255]),

            'tel_num.required' => __('validation.required', ['attribute' => 'tel']),
            'tel_num.min' => __('validation.min.string', ['attribute' => 'tel', 'max' => 11]),

            'address.required' => __('validation.required', ['attribute' => 'địa chỉ']),
            'address.max' => __('validation.max.string', ['attribute' => 'địa chỉ']),

            'is_active.required' => __('validation.required', ['attribute' => 'trạng thái']),
        ];
    }
}
