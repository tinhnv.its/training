<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseFormRequest;

class CreateUserRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:5,255',
            'email' => 'required|string|email|max:255|unique:'.TABLE_NAME_USERS,
            'password' => 'required|string|confirmed|min:5',
            'group_role' => 'required|string',
            'is_active' => 'required|integer|between:0,1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('validation.required', ['attribute' => 'tên']),
            'name.between' => __('validation.between.string', ['attribute' => 'tên', 'min' => 5, 'max'=> 255]),

            'email.required' => __('validation.required', ['attribute' => 'email']),
            'email.email' => __('validation.email', ['attribute' => 'email']),
            'email.unique' => __('validation.unique', ['attribute' => 'email']),
            'email.max' => __('validation.max.string', ['attribute' => 'email', 'max' => 255]),

            'password.required' => __('validation.required', ['attribute' => 'mật khẩu']),
            'password.min' => __('validation.min.string', ['attribute' => 'mật khẩu', 'min' => 5]),

            'is_active.integer' => __('validation.integer', ['attribute' => 'trạng thái']),
            'is_active.required' => __('validation.required', ['attribute' => 'trạng thái']),
        ];
    }
}
