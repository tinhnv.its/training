<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\BaseFormRequest;

class UpdateProductRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrRule = [
            'product_name' => 'required|string|between:5,255',
            'product_price' => 'required|numeric|min:0',
            'is_sales' => 'required|integer',
        ];
        if ($this->image) {
            $arrRule['image'] = 'required|image|mimes:jpeg,png,jpg|max:2048|dimensions:max_width=1024,max_height=1024';
        }
        return $arrRule;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_name.required' => __('validation.required', ['attribute' => 'tên']),
            'product_name.between' => __('validation.between.string', ['attribute' => 'password', 'min' => 5, 'max'=> 255]),

            'product_price.required' => __('validation.required', ['attribute' => 'giá bán']),
            'product_price.numeric' => __('validation.numeric', ['attribute' => 'giá bán']),
            'product_price.min' => __('validation.min.numeric', ['attribute' => 'giá bán']),

            'is_sales.required' => __('validation.required', ['attribute' => 'trạng thái']),

            'product_image.required' => __('validation.required', ['attribute' => 'hình ảnh']),
            'product_image.image' => __('validation.image', ['attribute' => 'hình ảnh']),
            'product_image.mimes' => __('validation.mimes', ['attribute' => 'hình ảnh', 'values' => 'jpeg,png,jpg']),
            'product_image.dimensions' => __('validation.dimensions', ['attribute' => 'hình ảnh', 'max' => 1024]),
        ];
    }
}
