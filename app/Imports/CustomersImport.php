<?php

namespace App\Imports;

use Illuminate\Validation\Rule;
use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class CustomersImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customer([
            'customer_name' => !empty($row['ten_khac_hang']) ? $row['ten_khac_hang'] : null,
            'email' => !empty($row['email']) ? $row['email'] : null,
            'tel_num' => !empty($row['telnum']) ? $row['telnum'] : null,
            'address' => !empty($row['dia_chi']) ? $row['dia_chi'] : null,
        ]);
    }

    public function rules(): array
    {
        return [
            '*.ten_khac_hang' => ['required', 'between:5,255'],
            '*.email' => ['required', 'email', 'max:255'],
            '*.telnum' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'max:11'],
            '*.dia_chi' => ['required', 'max:255'],
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required' => __('validation.required', ['attribute' => 'tên']),
            '0.between' => __('validation.between.string', ['attribute' => 'password', 'min' => 5, 'max'=> 255]),

            '1.required' => __('validation.required', ['attribute' => 'email']),
            '1.email' => __('validation.email', ['attribute' => 'email']),
            '1.max' => __('validation.max.string', ['attribute' => 'email', 'max' => 255]),

            '2.required' => __('validation.required', ['attribute' => 'tel']),
            '2.min' => __('validation.min.string', ['attribute' => 'tel', 'max' => 11]),

            '3.required' => __('validation.required', ['attribute' => 'địa chỉ']),
            '3.max' => __('validation.max.string', ['attribute' => 'địa chỉ']),
        ];
    }
}
