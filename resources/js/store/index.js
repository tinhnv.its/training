import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    isLoading: false,
    messageSnackbar: {
      isShow: false,
      message: '',
      color: ''
    }
  },
  mutations: {
    loading(state, isLoading) {
      state.isLoading = isLoading;
    },
    showMessageSuccess(state, message) {
      state.messageSnackbar.isShow = true;
      state.messageSnackbar.color = 'success';
      state.messageSnackbar.message = message;
    },
    showMessageError(state, message) {
      state.messageSnackbar.isShow = true;
      state.messageSnackbar.color = 'red accent-2';
      state.messageSnackbar.message = message;
    },
    closeMessage(state, {}) {
      state.messageSnackbar.isShow = false;
      state.messageSnackbar.color = '';
      state.messageSnackbar.message = '';
    },
  },
  actions: {}
})

export default store
