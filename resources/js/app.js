require("./bootstrap");

window.Vue = require("vue").default;

import Vuetify from "./plugins/vuetify";
import i18n from "./plugins/i18n";
import router from "./router";
import store from "./store/index";

import App from "./App.vue";

import "@mdi/font/css/materialdesignicons.css";

Vue.component("auth-layout", require("./layouts/Auth.vue").default);
Vue.component("dashboard-layout", require("./layouts/Dashboard.vue").default);

const app = new Vue({
  store,
  router,
  el: "#app",
  vuetify: Vuetify,
  i18n,
  render: (h) => h(App),
});
