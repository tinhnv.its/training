import Vue from "vue";
import VueRouter from "vue-router";

import LoginPage from "./pages/Login.vue";
import ProductListPage from "./pages/product/ProductList.vue";
import ProductDataPage from "./pages/product/ProductData.vue";
import CustomerPage from "./pages/Customer.vue";
import UserPage from "./pages/User.vue";

Vue.use(VueRouter);

function requireAuth(to, from, next) {
    let accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
        next();
    } else {
        next({
            name: "LoginPage", // back to safety route //
            query: { redirectFrom: to.fullPath },
        });
    }
};

const router = new VueRouter({
    mode: "history",
    linkExactActiveClass: "active",
    routes: [
        {
            path: "/login",
            name: "LoginPage",
            component: LoginPage,
        },
        {
            path: "/users",
            name: "UserPage",
            component: UserPage,
            beforeEnter: requireAuth,
        },
        {
            path: "/customers",
            name: "CustomerPage",
            component: CustomerPage,
            beforeEnter: requireAuth,
        },
        {
            path: "/products",
            name: "ProductListPage",
            component: ProductListPage,
            beforeEnter: requireAuth,
        },
        {
            path: "/products/data/:productId?",
            name: "ProductDataPage",
            component: ProductDataPage,
            props: true,
            beforeEnter: requireAuth,
        },
    ],
});

export default router;
