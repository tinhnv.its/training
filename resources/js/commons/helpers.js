export const getAccessToken = () => {
  return localStorage.getItem('accessToken')
}

export const echoLog = () => {
  return (
    process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging'
  );
}
