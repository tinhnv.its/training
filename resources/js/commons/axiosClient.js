import axios from "axios"
import { BASE_URL } from "./constants"
import * as objectUtil from './object'
import { getAccessToken, echoLog } from './helpers';

const axiosClient = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": 'application/json',
    Accept: 'application/json',
    Authorization: `Bearer ${getAccessToken()}`,
  },
});

// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    config.params = config.params ? objectUtil.keysToSnakeCase(config.params) : null
    config.data = config.data ? objectUtil.keysToSnakeCase(config.data) : null
    if (echoLog()) {
      console.log('Request config  ===>>  ', config)
    }
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosClient.interceptors.response.use(
  (response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (echoLog()) {
      console.log('Response data  ===>>  ', response)
    }
    return objectUtil.keysToCamelCase(response.data)
  },
  (error) => {
    if (error.response.status == 401) {
      window.location.href = '/login';
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export default axiosClient;
