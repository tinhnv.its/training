import * as _ from 'lodash'

export function keysToCamelCase(object) {
  if (!_.isPlainObject(object) && !_.isArray(object)) {
    return object;
  }

  let camelCaseObject = _.cloneDeep(object);

  if (_.isArray(camelCaseObject)) {
    return _.map(camelCaseObject, keysToCamelCase);
  } else {
    camelCaseObject = _.mapKeys(camelCaseObject, (value, key) => {
      // お知らせレイアウトuuid用 'uuid'+36文字(uuid32文字+ハイフン4つ)なら処理しない
      if (key.startsWith('uuid') && key.length === 40) return key;
      return _.camelCase(key);
    });
    // Recursively apply throughout object
    return _.mapValues(camelCaseObject, value => {
      if (_.isPlainObject(value)) {
        return keysToCamelCase(value);
      } else if (_.isArray(value)) {
        return _.map(value, keysToCamelCase);
      } else {
        return value;
      }
    });
  }
}

export function keysToSnakeCase(object) {
  if (!_.isPlainObject(object) && !_.isArray(object)) {
    return object;
  }

  let snakeCaseObject = _.cloneDeep(object);

  if (_.isArray(snakeCaseObject)) {
    return _.map(snakeCaseObject, keysToSnakeCase);
  } else {
    snakeCaseObject = _.mapKeys(snakeCaseObject, (value, key) => {
      // お知らせレイアウトuuid用 'uuid'+36文字(uuid32文字+ハイフン4つ)なら処理しない
      if (key.startsWith('uuid') && key.length === 40) return key;
      return key.replace(/([A-Z])/g, function(s) {
        return '_' + s.charAt(0).toLowerCase();
      });
    });

    // Recursively apply throughout object
    return _.mapValues(snakeCaseObject, value => {
      if (_.isPlainObject(value)) {
        return keysToSnakeCase(value);
      } else if (_.isArray(value)) {
        return _.map(value, keysToSnakeCase);
      } else {
        return value;
      }
    });
  }
}
