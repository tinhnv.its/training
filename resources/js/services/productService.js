import axiosClient from '../commons/axiosClient'

const productService = {
  getList(params) {
    const url = '/products'
    return axiosClient.get(url, {
      params
    })
  },

  getById(id) {
    const url = `/products/${id}`
    return axiosClient.get(url)
  },

  add(data) {
    const url = '/products'
    return axiosClient.post(url, data)
  },

  update(id, data) {
    const url = `/products/${id}`
    return axiosClient.post(url, data)
  },

  remove(id) {
    const url = `/products/${id}`
    return axiosClient.delete(url)
  }
}

export default productService
