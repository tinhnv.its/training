import axiosClient from '../commons/axiosClient'

const userService = {
  getList(params) {
    const url = '/users'
    return axiosClient.get(url, {
      params
    })
  },

  getById(id) {
    const url = `/users/${id}`
    return axiosClient.get(url)
  },

  add(data) {
    const url = '/users'
    return axiosClient.post(url, data)
  },

  update(id, data) {
    const url = `/users/${id}`
    return axiosClient.put(url, data)
  },

  updateStatus(id, data) {
    const url = `/users/update-status/${id}`
    return axiosClient.put(url, data)
  },

  remove(id) {
    const url = `/users/${id}`
    return axiosClient.delete(url)
  }
}

export default userService
