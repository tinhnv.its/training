import axiosClient from '../commons/axiosClient'

const authService = {
  login(data) {
    const url = '/auth/login'
    return axiosClient.post(url, data)
  },
}

export default authService
