import axiosClient from '../commons/axiosClient'

const customerService = {
  getList(params) {
    const url = '/customers'
    return axiosClient.get(url, {
      params
    })
  },

  getById(id) {
    const url = `/customers/${id}`
    return axiosClient.get(url)
  },

  add(data) {
    const url = '/customers'
    return axiosClient.post(url, data)
  },

  update(id, data) {
    const url = `/customers/${id}`
    return axiosClient.put(url, data)
  },

  updateStatus(id, data) {
    const url = `/customers/update-status/${id}`
    return axiosClient.put(url, data)
  },

  remove(id) {
    const url = `/customers/${id}`
    return axiosClient.delete(url)
  },

  exportCSV(params) {
    const url = '/customers/export'
    return axiosClient.get(url, {
      params
    })
  },

  importCSV(data) {
    const url = '/customers/import'
    return axiosClient.post(url, data)
  },
}

export default customerService
