<?php

return [
    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_required' => 'Please log in.',
    'login_wrong' => 'Wrong password or this account not approved yet.',
    'logout_success' => 'User successfully signed out',
];
