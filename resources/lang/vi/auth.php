<?php

return [
    'failed' => 'Những thông tin xác thực này không khớp với hồ sơ của chúng tôi.',
    'password' => 'Mật khẩu không chính xác.',
    'throttle' => 'Quá nhiều lần đăng nhập. Vui lòng thử lại sau :seconds giây.',
    'login_required' => 'Vui lòng đăng nhập.',
    'login_wrong' => 'Mật khẩu sai hoặc tài khoản chưa được phê duyệt.',
    'logout_success' => 'Tài khoản đăng xuất thành công.'
];
