<?php

use Symfony\Component\HttpFoundation\Response;

if (!function_exists('responseJson')) {
    function responseJson($code = Response::HTTP_NOT_FOUND, $data = null) {
        $json_data = [
            "code" => $code,
            "message" => trans('response.' . $code),
        ];

        if (in_array($code, [Response::HTTP_OK, Response::HTTP_CREATED])) {
            $json_data["data"] = $data;
        } else {
            $json_data["errors"] = $data;
        }

        return response()->json($json_data, $code);
    }
}
