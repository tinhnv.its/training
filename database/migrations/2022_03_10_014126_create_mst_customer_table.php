<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TABLE_NAME_CUSTOMER, function (Blueprint $table) {
            $table->increments('customer_id');
            $table->string('customer_name', 255);
            $table->string('email', 255);
            $table->string('tel_num', 14);
            $table->string('address', 255);
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TABLE_NAME_CUSTOMER);
    }
}
