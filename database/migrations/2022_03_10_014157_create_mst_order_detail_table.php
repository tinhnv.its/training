<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TABLE_NAME_ORDER_DETAIL, function (Blueprint $table) {
            $table->integer('order_id')->unique();
            $table->integer('detail_line');
            $table->string('product_id', 50);
            $table->integer('price_buy');
            $table->integer('quantity');
            $table->string('shop_id', 50);
            $table->integer('receiver_id');
            $table->timestamps();

            $table->primary(['order_id', 'detail_line']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TABLE_NAME_ORDER_DETAIL);
    }
}
